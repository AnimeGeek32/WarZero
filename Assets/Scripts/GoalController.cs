﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public enum GoalType
{
    GOAL_PLAYER,
    GOAL_AI
};

public class GoalController : MonoBehaviour {
    public GoalType goalType = GoalType.GOAL_PLAYER;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnTriggerEnter(Collider other)
	{
        if (other.tag == "Ball") {
            if (!FieldManager.Instance.GoalScored) {
                FieldManager.Instance.GoalScored = true;
                if (goalType == GoalType.GOAL_PLAYER) {
                    GameManager.Instance.ScoreRound(GameState.Team2Wins);
                    Debug.Log("Player's goal scored.");
                } else if (goalType == GoalType.GOAL_AI) {
                    GameManager.Instance.ScoreRound(GameState.Team1Wins);                    
                    Debug.Log("AI's goal scored.");
                }

                //StartCoroutine(AdvanceToNewRound());
            }
        }
	}

    // Temporary round reset
    // IEnumerator AdvanceToNewRound()
    // {
    //     yield return new WaitForSeconds(3.0f);
    //     SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    // }
}
