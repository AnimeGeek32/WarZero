﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponSoundManager : MonoBehaviour {

	[FMODUnity.EventRef]
	public string weaponSound;

	// Use this for initialization
	void OnWeapon ()
	{
		FMODUnity.RuntimeManager.PlayOneShot (weaponSound, transform.position);
	}
}
