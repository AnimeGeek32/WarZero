﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;      //Allows us to use UI.

public class PlayerHUDController : MonoBehaviour {

    public Image round1Win;   
    public Image round2Win;	
	public Text hasBallText;

	private bool _isAIBot = false;

	// Use this for initialization
	void Start () {
		
		if (gameObject.tag == "AI") {
			_isAIBot = true;
			Debug.Log("Set AI HUD");
		}

		Reset();
	}
	
	// Update is called once per frame
	void Update () {

		if (FieldManager.Instance.GoalScored == true) {

			if (_isAIBot == true) {
				if (GameManager.Instance.team2WinCount == 1) {
					round1Win.color = Color.green; 			
				} else if (GameManager.Instance.team2WinCount == 2) {
					round2Win.color = Color.green;
				}
			} else {
				if (GameManager.Instance.team1WinCount == 1) {
					round1Win.color = Color.green; 				
				} else if (GameManager.Instance.team1WinCount == 2) {
					round2Win.color = Color.green;
				}
			}

			return;
		}

		if (GameManager.Instance.gameState == GameState.InProgress) {
			if (FieldManager.Instance.currentPossession ==  BallPossession.Possession_AI && _isAIBot == true) {
				hasBallText.text = "Red Has The Ball"; 
			} 
			else if (FieldManager.Instance.currentPossession ==  BallPossession.Possession_Player && _isAIBot == false) {
				hasBallText.text = "Blue Has The Ball"; 
			}			
			else {
				hasBallText.text = "";
			}
		} 

	}

	void Reset () {
		round1Win.GetComponent<Image>().color = new Color32(255,255,225,90);
		round2Win.GetComponent<Image>().color = new Color32(255,255,225,90);
		hasBallText.text = "";		
	}

}
