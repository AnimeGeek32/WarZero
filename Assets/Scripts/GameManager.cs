﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;      //Allows us to use UI.

public enum GameState
{
    Initialize, Ready, InProgress, Team1Wins, Team2Wins, MatchOver
}

public class GameManager : MonoBehaviour
{

    // Properties
    public GameState gameState = GameState.Initialize;
    public int currentRound = 1;    

    public int team1WinCount = 0;
    public int team2WinCount = 0;    

	[FMODUnity.EventRef]
	public string winSound;

    private static GameManager _instance = null;

    private void Awake()
    {
        if (_instance != this)
            _instance = this;
    }

    public static GameManager Instance
    {
        get
        {
            return _instance;
        }
    }

    private void OnDestroy()
    {
        if (_instance == this)
            _instance = null;
    }

    // Use this for initialization
    void Start()
    {

        // Set initial UI
        currentRound = 1;
        gameState = GameState.Initialize;   
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyUp(KeyCode.Return) && gameState != GameState.InProgress)
        {
            if (gameState == GameState.Initialize) {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;

                gameState = GameState.Ready;
            } else if (gameState == GameState.Ready) {
                gameState = GameState.InProgress;
            } else if (gameState == GameState.Team1Wins || gameState == GameState.Team2Wins) {
                FieldManager.Instance.ResetPlayerPositions();                
                gameState = GameState.Ready;

            } else if (gameState == GameState.MatchOver) {
                StartCoroutine (ExitToMenu());
            }
        }


    }

    public void ScoreRound(GameState teamWon)
    {
        gameState = teamWon;
        currentRound += 1;

        if (gameState == GameState.Team1Wins)
        {

            team1WinCount += 1;

            if (team1WinCount == 2)
            {
                Debug.Log("Team blue scores. Next round:" + currentRound);
                gameState = GameState.MatchOver;
            } else {
                Debug.Log("Round Over!");
				FMODUnity.RuntimeManager.PlayOneShot (winSound, transform.position);
                StartCoroutine (RestartRoundInSecs(5));
            }
        }
        else if (gameState == GameState.Team2Wins)
        {

            team2WinCount += 1;

            if (team2WinCount == 2) {

                Debug.Log("Team red scores. Next round:" + currentRound);                
                gameState = GameState.MatchOver;
            } else {                

                Debug.Log("Round Over!"); 
				FMODUnity.RuntimeManager.PlayOneShot (winSound, transform.position);
                StartCoroutine (RestartRoundInSecs(5));
            }
        }
    }

    public void ResetRound() {
        
        Debug.Log("Reset for Round " + currentRound);
        //make the goal reset
        FieldManager.Instance.GoalScored = false;
        
        //reset game state
         gameState = GameState.Ready;
    }

    public void LeaveGame() {            
        SceneManager.LoadScene(0);
    }
    //IEnumerator

    IEnumerator ExitToMenu()
    {
        yield return new WaitForSeconds(10);
        SceneManager.LoadScene(0);
    }

    IEnumerator RestartGameInSecs(float sec)
    {
        yield return new WaitForSeconds(sec);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    IEnumerator RestartRoundInSecs(float sec)
    {
        yield return new WaitForSeconds(sec);
        ResetRound();
    }

}

