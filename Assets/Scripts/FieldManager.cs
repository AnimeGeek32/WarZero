﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BallPossession
{
    Possession_None,
    Possession_Player,
    Possession_AI
};

public class FieldManager : MonoBehaviour {
    public GameObject ball;
    public Transform playerGoalTrigger;
    public Transform aiGoalTrigger;
    public BallPossession currentPossession = BallPossession.Possession_None;

    [Header("Automated Param - Do Not Manually Assign")]
    public GameObject currentBallOwner = null;
    public GameObject playerChar;
    //public GameObject aiChar;
    public List<GameObject> playerTeamBots;
    public List<GameObject> opponentTeamBots;

    private static FieldManager _instance = null;

    private bool _roundStarted = false;
    private bool _goalScored = false;

    private List<Vector3> team1InitialPositions;
    private List<Quaternion> team1InitialRotations;

    private List<Vector3> team2InitialPositions;
    private List<Quaternion> team2InitialRotations;    
    private Vector3 ballInitialPosition;

	private void Awake()
	{
        if (_instance != this)
            _instance = this;

        playerTeamBots = new List<GameObject>();
        opponentTeamBots = new List<GameObject>();
	}

	// Use this for initialization
	void Start () {
        _goalScored = false;
        _roundStarted = false;
        team1InitialPositions = new List<Vector3>();
        team1InitialRotations = new List<Quaternion>();

        team2InitialPositions = new List<Vector3>();
        team2InitialRotations = new List<Quaternion>();

        ballInitialPosition = ball.transform.position;

        team1InitialPositions.Add(playerChar.transform.position);
        playerChar.gameObject.GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl>().enabled = false;
        foreach (GameObject friendlyBot in playerTeamBots)
        {
            team1InitialPositions.Add(friendlyBot.transform.position);
            team1InitialRotations.Add(friendlyBot.transform.rotation);
            friendlyBot.GetComponent<UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>().enabled = false;
        }
        foreach (GameObject opponentBot in opponentTeamBots)
        {
            team2InitialPositions.Add(opponentBot.transform.position);
            team2InitialRotations.Add(opponentBot.transform.rotation);
            opponentBot.GetComponent<UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>().enabled = false;
        }
	}
	
	// Update is called once per frame
	void Update () {

		if (GameManager.Instance.gameState == GameState.InProgress && _roundStarted == false) {
            playerChar.gameObject.GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl>().enabled = true;
            foreach (GameObject friendlyBot in playerTeamBots)
            {
                friendlyBot.GetComponent<UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>().enabled = true;
            }
            foreach (GameObject opponentBot in opponentTeamBots)
            {
                opponentBot.GetComponent<UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>().enabled = true;
            }
            _roundStarted = true;
        } else if (GameManager.Instance.gameState == GameState.Team1Wins ||  GameManager.Instance.gameState == GameState.Team2Wins) {
            playerChar.gameObject.GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl>().enabled = false;
            foreach (GameObject friendlyBot in playerTeamBots)
            {
                friendlyBot.GetComponent<UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>().enabled = false;
            }
            foreach (GameObject opponentBot in opponentTeamBots)
            {
                opponentBot.GetComponent<UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>().enabled = false;
            }
            _roundStarted = false;
        }

	}

    public static FieldManager Instance
    {
        get
        {
            return _instance;
        }
    }

    public bool GoalScored {
        get
        {
            return _goalScored;
        }
        set
        {
            _goalScored = value;
        }
    }

	private void OnDestroy()
	{
        if (_instance == this)
            _instance = null;
	}

    public void TellAIToGoToBall()
    {
        foreach (GameObject friendlyBot in playerTeamBots) {
            friendlyBot.GetComponent<UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>().SetTarget(ball.transform);
        }
        foreach (GameObject opponentBot in opponentTeamBots) {
            opponentBot.GetComponent<UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>().SetTarget(ball.transform);
        }
    }

    public void TellAIWhoOwnsTheBall(BallPossession ballOwningTeam)
    {
        if (ballOwningTeam == BallPossession.Possession_Player) {
            foreach (GameObject friendlyBot in playerTeamBots) {
                if (friendlyBot == currentBallOwner) {
                    friendlyBot.GetComponent<UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>().SetTarget(aiGoalTrigger.transform);
                } else {
                    friendlyBot.GetComponent<UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>().SetTarget(currentBallOwner.transform);
                }
            }
            foreach (GameObject opponentBot in opponentTeamBots)
            {
                opponentBot.GetComponent<UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>().SetTarget(currentBallOwner.transform);
            }
        } else {
            foreach (GameObject friendlyBot in playerTeamBots)
            {
                friendlyBot.GetComponent<UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>().SetTarget(currentBallOwner.transform);
            }
            foreach (GameObject opponentBot in opponentTeamBots) {
                if (opponentBot == currentBallOwner)
                {
                    opponentBot.GetComponent<UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>().SetTarget(playerGoalTrigger.transform);
                }
                else
                {
                    opponentBot.GetComponent<UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>().SetTarget(currentBallOwner.transform);
                }
            }
        }
    }

    public void ResetPlayerPositions()
    {
        //reset player positions
        playerChar.transform.position = team1InitialPositions[0];
        for (int i = 0; i < playerTeamBots.Count; i++) {
            playerTeamBots[i].transform.position = team1InitialPositions[i + 1];
            playerTeamBots[i].transform.rotation = team1InitialRotations[i + 1];
        }
        for (int j = 0; j < opponentTeamBots.Count; j++) {
            opponentTeamBots[j].transform.position = team2InitialPositions[j];
            opponentTeamBots[j].transform.rotation = team2InitialRotations[j];
        }
        ball.transform.position = ballInitialPosition;

        playerChar.gameObject.GetComponent<UnityStandardAssets.Characters.ThirdPerson.ThirdPersonUserControl>().enabled = false;
        foreach (GameObject friendlyBot in playerTeamBots)
        {
            friendlyBot.GetComponent<UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>().enabled = false;
        }
        foreach (GameObject opponentBot in opponentTeamBots)
        {
            opponentBot.GetComponent<UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>().enabled = false;
        }

        TellAIToGoToBall();
    }
}
