﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TeamAllegiance
{
    Team_Player,
    Team_Opponent
};

public class BallHolder : MonoBehaviour {
	[FMODUnity.EventRef]
	public string pickupSound;
    public Transform ballHolderTransform;
    public bool isHoldingBall = false;
    public TeamAllegiance teamAllegiance = TeamAllegiance.Team_Player;

    private bool _isAiBot = false;
    UnityStandardAssets.Characters.ThirdPerson.AICharacterControl _aiControl;

	private void Awake()
	{
        if (gameObject.tag == "Player")
        {
            FieldManager.Instance.playerChar = gameObject;
        }
        else if (gameObject.tag == "AI")
        {
            if (teamAllegiance == TeamAllegiance.Team_Player)
            {
                FieldManager.Instance.playerTeamBots.Add(gameObject);
            }
            else
            {
                FieldManager.Instance.opponentTeamBots.Add(gameObject);
            }
            _isAiBot = true;
            _aiControl = GetComponent<UnityStandardAssets.Characters.ThirdPerson.AICharacterControl>();
            _aiControl.SetTarget(FieldManager.Instance.ball.transform);
        }
	}

	// Use this for initialization
	void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        if (_isAiBot) {
            if (FieldManager.Instance.GoalScored) {
                if (_aiControl.target != null) {
                    _aiControl.SetTarget(null);
                }
            }
        }

        if (isHoldingBall) {
            FieldManager.Instance.ball.transform.localPosition = Vector3.zero;
        }
	}

	private void OnCollisionEnter(Collision collision)
	{
        if (collision.collider.tag == "Ball") {
            if (!isHoldingBall && !FieldManager.Instance.GoalScored)
            {
				FMODUnity.RuntimeManager.PlayOneShot (pickupSound);
                HoldBall();
            }
        } else if (collision.collider.tag == "Player" || collision.collider.tag == "AI") {
            BallHolder targetBallHolder = collision.gameObject.GetComponent<BallHolder>();
            if (targetBallHolder != null) {
                if (targetBallHolder.teamAllegiance != this.teamAllegiance)
                {
                    if (targetBallHolder.isHoldingBall)
                    {
                        Vector3 pushDir = GetComponent<Rigidbody>().velocity;
                        targetBallHolder.DropBall();
                        collision.collider.GetComponent<Rigidbody>().AddForce(pushDir);
                        FieldManager.Instance.ball.GetComponent<Rigidbody>().AddForce(pushDir * 2);
                    }
                }
            }
        }
	}

	private void OnTriggerEnter(Collider other)
	{
        if (other.tag == "Goal")
        {
            if (isHoldingBall)
            {
                DropBall();
            }
        }
	}

	public void HoldBall()
    {
        FieldManager.Instance.currentBallOwner = gameObject;
        if (teamAllegiance == TeamAllegiance.Team_Opponent) {
            FieldManager.Instance.currentPossession = BallPossession.Possession_AI;
            FieldManager.Instance.TellAIWhoOwnsTheBall(BallPossession.Possession_AI);
        } else {
            FieldManager.Instance.currentPossession = BallPossession.Possession_Player;
            FieldManager.Instance.TellAIWhoOwnsTheBall(BallPossession.Possession_Player);
        }
        Rigidbody ballRigidbody = FieldManager.Instance.ball.GetComponent<Rigidbody>();
        FieldManager.Instance.ball.GetComponent<Collider>().enabled = false;
        FieldManager.Instance.ball.transform.position = ballHolderTransform.position;
        FieldManager.Instance.ball.transform.parent = ballHolderTransform;
        ballRigidbody.isKinematic = true;
        ballRigidbody.detectCollisions = false;
        ballRigidbody.useGravity = false;
        ballRigidbody.velocity = Vector3.zero;
        isHoldingBall = true;
    }

    public void DropBall()
    {
        FieldManager.Instance.currentBallOwner = null;
        FieldManager.Instance.currentPossession = BallPossession.Possession_None;
        FieldManager.Instance.TellAIToGoToBall();
        Rigidbody ballRigidbody = FieldManager.Instance.ball.GetComponent<Rigidbody>();
        FieldManager.Instance.ball.GetComponent<Collider>().enabled = true;
        FieldManager.Instance.ball.transform.parent = null;
        ballRigidbody.isKinematic = false;
        ballRigidbody.detectCollisions = true;
        ballRigidbody.useGravity = true;
        isHoldingBall = false;
    }
    
}
