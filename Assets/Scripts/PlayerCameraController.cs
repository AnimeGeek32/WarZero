﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCameraController : MonoBehaviour {
    public float minX = -90f;
    public float maxX = 90f;
    public float rotationSpeed = 10f;
    public float dampingTime = 0.2f;
    public string axisXName = "Mouse X";
    public string axisYName = "Mouse Y";
    public Transform target;
    public float distanceFromTarget = 10f;
    public float heightAboveTarget = 4f;

    private bool _hideCursor = true;
    private bool _firstCyclePassed = false;
    private Vector3 m_TargetAngles;
    private Vector3 m_FollowAngles;
    private Vector3 m_FollowVelocity;
    private Quaternion m_OriginalRotation;

	// Use this for initialization
	void Start () {
        m_TargetAngles = Vector3.zero;
        m_FollowAngles = Vector3.zero;
        m_FollowVelocity = Vector3.zero;

        // Obtain current local rotation
        m_OriginalRotation = transform.localRotation;
        m_OriginalRotation.eulerAngles = new Vector3(0f, m_OriginalRotation.eulerAngles.y, m_OriginalRotation.z);

        // Lock and hide cursor
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        _hideCursor = true;
	}

	private void OnDisable()
	{
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
	}

    void ToggleCursor()
    {
        _hideCursor = !_hideCursor;
        Cursor.lockState = _hideCursor ? CursorLockMode.Locked : CursorLockMode.None;
        Cursor.visible = !_hideCursor;
    }

	// Update is called once per frame
	void Update () {
        if (!target)
            return;

        float wantedHeight = target.position.y + heightAboveTarget;

        // we make initial calculations from the original local rotation
        transform.localRotation = m_OriginalRotation;

        float inputH = Input.GetAxis(axisXName);
        float inputV = Input.GetAxis(axisYName);

        if (Input.GetKeyUp(KeyCode.Escape)) {
            ToggleCursor();
        }

        // wrap values to avoid springing quickly the wrong way from positive to negative
        if (m_TargetAngles.y > 180)
        {
            m_TargetAngles.y -= 360;
            m_FollowAngles.y -= 360;
        }
        if (m_TargetAngles.x > 180)
        {
            m_TargetAngles.x -= 360;
            m_FollowAngles.x -= 360;
        }
        if (m_TargetAngles.y < -180)
        {
            m_TargetAngles.y += 360;
            m_FollowAngles.y += 360;
        }
        if (m_TargetAngles.x < -180)
        {
            m_TargetAngles.x += 360;
            m_FollowAngles.x += 360;
        }

        // with mouse input, we have direct control with no springback required.
        m_TargetAngles.y += inputH * rotationSpeed;
        m_TargetAngles.x += inputV * rotationSpeed;

        // Force camera's x orientation at 0 when moving mouse horizontally at beginning
        if (!_firstCyclePassed && (System.Math.Abs(inputV) > Mathf.Epsilon)) {
            m_TargetAngles.x = 0;
            _firstCyclePassed = true;
        }

        // clamp values to allowed range
        m_TargetAngles.x = Mathf.Clamp(m_TargetAngles.x, minX, maxX);

        // smoothly interpolate current values to target angles
        m_FollowAngles = Vector3.SmoothDamp(m_FollowAngles, m_TargetAngles, ref m_FollowVelocity, dampingTime);

        // update the actual gameobject's rotation
        transform.localRotation = m_OriginalRotation * Quaternion.Euler(-m_FollowAngles.x, m_FollowAngles.y, 0);

        // Set the position of the camera on the x-z plane to:
        // distance meters behind the target
        Vector3 targetPosWithAdjustedHeight = new Vector3(target.position.x, wantedHeight, target.position.z);
        transform.position = targetPosWithAdjustedHeight;
        transform.position -= transform.localRotation * Vector3.forward * distanceFromTarget;

        // Always look at the target
        transform.LookAt(targetPosWithAdjustedHeight);
	}
}
