﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootStepManager : MonoBehaviour {

	[FMODUnity.EventRef]
	public string footStepSound;

	// Use this for initialization
	void OnFootStep ()
	{
		FMODUnity.RuntimeManager.PlayOneShot (footStepSound, transform.position);
	}
}
