﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;      //Allows us to use UI.

public class MainMenuController : MonoBehaviour {

    //UI
    public Image logoImage;
    public Image readyImage;
    public Image engageImage;
    public Image team1WinsImage;
    public Image team2WinsImage;
    public Image lineImage;

    //TEXT
    public Text roundText;
    public Text statusText;
    public Text creditsText;	

	private Image mainMenuImage;

    private static MainMenuController _instance = null;

	private Color _checkActiveLineColor;

    private void Awake()
    {
        if (_instance != this)
            _instance = this;
    }

    public static MainMenuController Instance
    {
        get
        {
            return _instance;
        }
    }

    private void OnDestroy()
    {
        if (_instance == this)
            _instance = null;
    }

	// Use this for initialization
	void Start () {
		
		mainMenuImage = gameObject.GetComponent<Image>();

		SetStartUI();
	}
	
	// Update is called once per frame
	void Update () {
		
		switch (GameManager.Instance.gameState)
		{
			case GameState.Initialize:
				//				
				break;
			case GameState.Ready:	
				ShowReadyUI();	
				break;				
			case GameState.InProgress:
				HideAllUI();
				break;
			case GameState.Team1Wins:
				ShowTeam1WinUI();
				break;
			case GameState.Team2Wins:    
				ShowTeam2WinUI();					
				break;	
			case GameState.MatchOver:
				Debug.Log("Match is over.");            
				ShowMatchOverUI();
				break;								
			default:
				Debug.Log("default");
				break;
		}

	}

	public void SetStartUI() {
		
		_checkActiveLineColor = new Color(lineImage.color.r, lineImage.color.g, lineImage.color.b, 1.0f);

		Debug.Log("Initial HUD " + _checkActiveLineColor.a);

		HideAllUI();

		logoImage.color = new Color(logoImage.color.r, logoImage.color.g, logoImage.color.b, 1.0f);        
		lineImage.color = new Color(lineImage.color.r, lineImage.color.g, lineImage.color.b, 1.0f); 
		statusText.text = "Press Enter To Start Match";
		roundText.text = "Round 1";
		creditsText.text = "Credits: Jon Crissey, Daniel Jung, Alex Cole, Kotaro Fujita";
		mainMenuImage.color = new Color(mainMenuImage.color.r, mainMenuImage.color.g, mainMenuImage.color.b, 0.6f);	
	}

    public void ShowReadyUI() {

		//DON'T SHOW IF ITS ALREADY SHOWN
		if (lineImage.color.a == _checkActiveLineColor.a && creditsText.text == "")
		{
			return;
		}

		HideAllUI();		      

        readyImage.color = new Color(readyImage.color.r, readyImage.color.g, readyImage.color.b, 1.0f);
		lineImage.color = new Color(lineImage.color.r, lineImage.color.g, lineImage.color.b, 1.0f);
		statusText.text = "Press Enter To Begin";
		creditsText.text = "";

		var round = GameManager.Instance.currentRound;
		roundText.text = "Round " + round;		

		mainMenuImage.color = new Color(mainMenuImage.color.r, mainMenuImage.color.g, mainMenuImage.color.b, 0.6f);				

		Debug.Log("Show Ready UI: " + lineImage.color.a);			
	}

	public void ShowTeam1WinUI() {

		Debug.Log("Show Team 1 Win HUD: " + lineImage.color.a);

		//DON'T SHOW IF ITS ALREADY SHOWN
		// if (lineImage.color.a == _checkActiveLineColor.a && creditsText.text == "")
		// {
		// 	return;
		// }

		HideAllUI();		

		Debug.Log("Show Team 1 Win HUD");		
        team1WinsImage.color = new Color(team1WinsImage.color.r, team1WinsImage.color.g, team1WinsImage.color.b, 1.0f);
		lineImage.color = new Color(lineImage.color.r, lineImage.color.g, lineImage.color.b, 1.0f);
		statusText.text = "Press Enter To Start Next Round";

		mainMenuImage.color = new Color(mainMenuImage.color.r, mainMenuImage.color.g, mainMenuImage.color.b, 0.6f);				
	}

	public void ShowTeam2WinUI() {
		
		Debug.Log("Show Team 2 Win HUD: " + lineImage.color.a);

		//DON'T SHOW IF ITS ALREADY SHOWN
		// if (lineImage.color.a == _checkActiveLineColor.a && creditsText.text == "")
		// {
		// 	return;
		// }

		HideAllUI();		
		Debug.Log("Show Team 2 Win HUD");	
        team2WinsImage.color = new Color(team2WinsImage.color.r, team2WinsImage.color.g, team2WinsImage.color.b, 1.0f);
		lineImage.color = new Color(lineImage.color.r, lineImage.color.g, lineImage.color.b, 1.0f);
		statusText.text = "Press Enter To Start Next Round";

		mainMenuImage.color = new Color(mainMenuImage.color.r, mainMenuImage.color.g, mainMenuImage.color.b, 0.6f);
	}

	public void ShowMatchOverUI() {

		//DON'T SHOW IF ITS ALREADY SHOWN
		if (lineImage.color.a == _checkActiveLineColor.a && creditsText.text == "")
		{
			return;
		}

		HideAllUI();			

		lineImage.color = new Color(lineImage.color.r, lineImage.color.g, lineImage.color.b, 1.0f);

		if (GameManager.Instance.team1WinCount == 2) {
			Debug.Log("Show Match Over. Winner: Team Blue");
			 team1WinsImage.color = new Color(team1WinsImage.color.r, team1WinsImage.color.g, team1WinsImage.color.b, 1.0f);
		} else if (GameManager.Instance.team2WinCount == 2) {
			Debug.Log("Show Match Over. Winner: Team Red");			
			 team2WinsImage.color = new Color(team2WinsImage.color.r, team2WinsImage.color.g, team2WinsImage.color.b, 1.0f);
		}
       	
	   statusText.text = "Game over. Will Restart In 10 secs.";	

		mainMenuImage.color = new Color(mainMenuImage.color.r, mainMenuImage.color.g, mainMenuImage.color.b, 0.6f);
		
	}

    //private helpers
    private void HideAllUI() {

		mainMenuImage.color = new Color(mainMenuImage.color.r, mainMenuImage.color.g, mainMenuImage.color.b, 0.0f);
        logoImage.color = new Color(logoImage.color.r, logoImage.color.g, logoImage.color.b, 0.0f);        
        readyImage.color = new Color(readyImage.color.r, readyImage.color.g, readyImage.color.b, 0.0f);
        engageImage.color = new Color(engageImage.color.r, engageImage.color.g, engageImage.color.b, 0.0f);
        lineImage.color = new Color(lineImage.color.r, lineImage.color.g, lineImage.color.b, 0.0f);        
        team1WinsImage.color = new Color(team1WinsImage.color.r, team1WinsImage.color.g, team1WinsImage.color.b, 0.0f);
        team2WinsImage.color = new Color(team2WinsImage.color.r, team2WinsImage.color.g, team2WinsImage.color.b, 0.0f);
		statusText.text = "";
		creditsText.text = "";
    }

}

